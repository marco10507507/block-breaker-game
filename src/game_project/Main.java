package game_project;

import javax.swing.JFrame;

public class Main {
	
	public static void main(String[] args){
		JFrame frame = new JFrame("Project_1");
		
		BlockBreakerPanel panel = new BlockBreakerPanel();
		
		frame.getContentPane().add(panel);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setVisible(true);
		frame.setSize(485, 600);
		
		frame.setResizable(false);
		
	}

}
